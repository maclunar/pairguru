Rails.application.routes.draw do
  devise_for :users

  root "home#welcome"

  resources :genres, only: :index do
    member do
      get "movies"
    end
  end

  resources :movies, only: [:index, :show] do
    member do
      get :send_info
    end

    collection do
      get :export
    end
  end

  resources :users, only: [:index, :show] do
    member do
      post :invite
    end
  end

  resources :invitations, only: [:index] do
    member do
      post :accept
    end
  end
end
