# == Schema Information
#
# Table name: invitations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  friend_id  :integer
#  state      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Invitation < ApplicationRecord
  belongs_to :user
  belongs_to :friend, class_name: 'User'

  enum state: [:active, :accepted, :rejected]

  validates_uniqueness_of :friend, scope: :user

  def accept!
    self.update(state: :accepted)
  end
end
