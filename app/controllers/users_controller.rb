class UsersController < ApplicationController
  before_action :load_user, only: [:show, :invite]

  def index
    @users = User.all
  end

  def show; end

  def invite
    @invitation = Invitation.new(user: current_user, friend: @user, state: :active)

    if @invitation.save
      flash[:notice] = 'The user has been invited!'
    else
      flash[:error] = 'Something went wrong'
    end

    redirect_to @user
  end

  private

  def load_user
    @user = User.find(params[:id])
  end
end
