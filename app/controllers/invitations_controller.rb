class InvitationsController < ApplicationController
  before_action :authenticate_user!, only: [:index, :accept]
  before_action :load_invitation, only: :accept

  def index
    @active_invitations = current_user.invitations.active
    @accepted_invitations = current_user.invitations.accepted
    @rejected_invitations = current_user.invitations.rejected
  end

  def accept
    @invitation.accept!
    redirect_to invitations_path, notice: 'Invitation accepted!'
  end

  private

  def load_invitation
    @invitation = Invitation.find(params[:id])
  end
end
