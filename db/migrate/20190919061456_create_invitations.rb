class CreateInvitations < ActiveRecord::Migration[6.0]
  def change
    create_table :invitations do |t|
      t.references :user, foreign_key: true
      t.references :friend, foreign_key: false
      t.integer :state
      t.timestamps
    end
    add_foreign_key :invitations, :users, column: :friend_id
  end
end
